# salt.eligundry.com

This is a collection of [SaltStack][1] states that allow me to replicate my
development environments across my machines consistently.

I gave a talk about the reasoning behind this at [NYC Vim][2].

## Installation

1. Install the Salt Minion
2. Set the master to `eligundry.ninja`
3. Authenticate the keys
4. `./init.sh <device-name>`

[1]: https://saltstack.com/
[2]: https://eligundry.com/talks/dots/
