bearededspice:
  pkg.installed:
    - name: caskroom/cask/beardedspice

cyberduck:
  pkg.installed:
    - name: caskroom/cask/cyberduck

discord:
  pkg.installed:
    - name: caskroom/cask/discord

dropbox:
  pkg.installed:
    - name: caskroom/cask/dropbox

firefox:
  pkg.installed:
    - name: caskroom/cask/firefox

fliqlo:
  pkg.installed:
    - name: caskroom/cask/fliqlo

gimp:
  pkg.installed:
    - name: caskroom/cask/gimp

google-chrome:
  pkg.installed:
    - name: caskroom/cask/google-chrome

keepingyouawake:
  pkg.installed:
    - name: caskroom/cask/keepingyouawake

licecap:
  pkg.installed:
    - name: caskroom/cask/licecap

slack:
  pkg.installed:
    - name: caskroom/cask/slack

soundflower:
  pkg.installed:
    - name: caskroom/cask/soundflower

spotify:
  pkg.installed:
    - name: caskroom/cask/spotify

spotify-notifications:
  pkg.installed:
    - name: caskroom/cask/spotify-notifications

transmission:
  pkg.installed:
    - pkgs:
      - transmission
      - caskroom/cask/transmission

veracrypt:
  pkg.installed:
    - name: caskroom/cask/veracrypt

viscosity:
  pkg.installed:
    - name: caskroom/cask/viscosity
